\chapter{Backend}
	Das Backend ist das Herzstück einer jeden Anwendung. Es muss hochverfügbar und enorm fehlertolerant sein. Aus diesem Grund haben wir uns für Technologien entschieden, die Open-Source-Software sind und eine entsprechend große Verbreitung haben. Weiter war es von Anfang an wichtig, trotz der großen Abhängigkeit zum Backend die Entwicklung der anderen Teile nicht zu blockieren. Es wurden daher frühzeitig Modelle und Protokolle erstellt, die bereits vor der Fertigstellung gemockt werden konnten.
	\section{Technologiebeschreibung}
		\subsection{Spring Boot}
			Für die Implementierung des REST-Backends haben wir auf das Spring Framework gesetzt. Genauer gesagt, das Spring \emph{Boot} Framework. Das Wort "Boot" steht hierbei sinngemäß für "bootstrap", was uns viel Konfigurationsarbeit abgenommen hat. Alle Standard Beans und Factories waren bereits initialisiert und konnten ohne weitere Konfiguration genutzt werden. 
			
			Es wurden im Projektverlauf auch noch diverse Erweiterungen des Frameworks genutzt.
			\begin{itemize}
				\item \textbf{org.springframework.boot:spring-boot-starter-web} bringt einen integrierten Tomcat Application Server mit und ermöglicht das Verarbeiten von Webanfragen.
				\item \textbf{org.springframework.boot:spring-boot-starter-actuator} wird genutzt, um Endpoints für Diagnose freizuschalten.
				\item \textbf{org.springframework.boot:spring-boot-starter-data-jpa} bringt alle nötigen Abhängigkeiten, um mit der Java Persistence API Daten in einer Datenbank abzulegen.
				\item \textbf{org.springframework.boot:spring-boot-starter-data-rest} bietet Möglichkeiten, sehr leicht Datenbank Entitäten als HTTP REST Ressourcen bereitzustellen.
				\item \textbf{org.springframework.boot:spring-boot-starter-security} wird später zusammen mit der Authentifizierung über JWT genutzt.
				\item \textbf{org.springframework.boot:spring-boot-starter-test} bringt Möglichkeiten, leichtgewichtig Unit Tests für Webanwendungen zu schreiben.
			\end{itemize}
		
			Zur weiteren Reduktion des "Boilerplate Codes" wurde zusätzlich noch das Lombok Framework\footnote{\url{https://projectlombok.org/}} genutzt. Es bietet die Möglichkeit, Getter und Setter sowie diverse Konstruktoren für Datenklassen zu generieren. Dadurch konnten die Datenklassen um etwa 80\% in der Große reduziert werden, dies fördert die Lesbarkeit und vermeidet auch Leichtsinnsfehler.
		\subsection{MariaDB}
			Als Datenbank wurde MariaDB eingesetzt. MariaDB ist die quelloffene Entwicklung der MySQL Datenbank und nimmt deshalb alle Befehle an, die auch MySQL annimmt. Als Alternative stand noch Postgres zur Auswahl, da wir aber keine der erweiterten Funktionen von Postgres brauchten, fiel die Auswahl auf MariaDB. MariaDB musste auf keinem Entwicklungsrechner installiert werden, da immer das offizielle Dockerimage\footnote{\url{https://hub.docker.com/_/mariadb}} genutzt wurde.
		\subsection{Weitere Open Source Software}
			Eine weitere Bibliothek, die für die Authentifizierung benutzt wird, ist die Java-JWT Implementation von Auth0. Sowie die H2 In-Memory Datenbank. Diese zweite Datenbank wird während der Entwicklung genutzt, um schnell homogene Beispieldaten zu laden und Tests auf diesen durchzuführen.
		\subsection{Spezielles Setup}
			Um produktiv zu arbeiten, mussten noch weitere Tools genutzt werden. Dazu zählt primär die IntelliJ IDEA Ultimate Entwicklungsumgebung\footnote{\url{https://www.jetbrains.com/de-de/idea/}}. Diese IDE hat sehr viele Integrationen für das Spring Framework, als auch mit unseren Docker-Setup. Es wird dadurch möglich, ausschließlich in der IDE zu arbeiten, ohne weitere Kommandozeilenfenster.
			
			Das zweite wichtige Programm war der REST-Client Insomnia REST\footnote{\url{https://insomnia.rest/}}, welcher alle Möglichkeiten bietet, um REST APIs zu testen und Testabfragen auszuführen.
	\section{Umsetzung}
		\subsection{Spring Entities}
			\begin{figure}[H]
				\centering
				\includegraphics[width=\linewidth]{img/backend/er-modell.png}
				\caption{ER-Modell}
				\label{fig:er-modell}
			\end{figure}
		
			Das ER-Modell in Abbildung \ref{fig:er-modell} zeigt die komplette Hierarchie, wie sie unserem Konzept entspricht. Wir legen diese Definition aber nicht selbst in SQL an, sondern lassen Java Hibernate dies für uns tun. Die Grundstruktur der gespeicherten Daten ist wie folgt zu verstehen:
			\begin{itemize}
				\item Der \textbf{TimetrackUser} ist die Grundstruktur, die alle anderen Daten des Users zusammenhält. Sie speichert allgemeine Nutzerdaten und hält Referenzen auf die \textbf{Role} des Nutzers, seine \textbf{Location} und alle ihm gehörenden \textbf{TimetrackAccounts}.
				\item Die \textbf{Role} sollte ursprünglich erlauben, zwischen einem Admins und einem normalen Nutzers zu unterscheiden, aus Zeitgründen wurde dies aber weggelassen. Die Grundstruktur ist dennoch implementiert, allerdings so, dass jeder Nutzer automatisch Administrator ist.
				\item Die \textbf{Location} Entität speichert den Geofence des Nutzers. Diese Daten werden ausschließlich von der Android App genutzt, um beim Einloggen den Geofence zu setzen.
				\item Der \textbf{TimetrackAccount} ist die zweite große Struktur, die alle \textbf{TimeRecords} des Nutzers verwaltet. Jeder Nutzer kann mehrere \textbf{TimetrackAccounts} besitzen, aber jeder Account kann nur einem Nutzer gehören.
				\item Jede getrackte Zeitspanne wird in einem \textbf{TimeRecord} abgespeichert. Dieser Record speichert einen Typ sowie das Start- und Enddatum. Der Typ kann entweder "PAID" oder "BREAK" sein. Jeder Record gehört zu genau einem TimetrackAccount.
			\end{itemize}
			
			Die Umsetzung in Java wird nun am Beispiel des TimetrackUsers und des TimetrackAccounts gezeigt.
			
			\lstinputlisting[language=Java,caption=TimetrackUser,firstline=10]{../backend/src/main/java/de/hft/geotime/entities/TimetrackUser.java}
			
			Die komplette Klasse ist durch die Lombok Integration sehr klein gehalten. Alles weitere wird durch Annotationen geregelt, einige Beispiele sind hier:
			\begin{itemize}
				\item[] \textbf{@Entity} markiert die Klasse als speicherbar in der Datenbank.
				\item[] \textbf{@ManyToOne} markiert das Attribut als Fremdschlüssenrelation aus einer anderen Tabelle.
				\item[] \textbf{@Id} zeichnet den Primärschlüssel der Tabelle aus.
				\item[] \textbf{@Column} setzt spezielle Attribute für die Spalte in der Datenbank.
			\end{itemize}
			
			Die TimetrackAccounts haben zusätzlich noch die Eigenschaft, dass sie sich selbst rekursiv löschen, wenn der zugehörige User gelöscht wird. Selbiges gilt auch für die Records, wenn der zugehörige Account gelöscht wird.
			
			\lstinputlisting[language=Java,caption=TimetrackAccount,firstline=11]{../backend/src/main/java/de/hft/geotime/entities/TimetrackAccount.java}
			
		\subsection{Sicherheit durch JWT}
			Da wird die Web App im Laufe des Projekts auch öffentlich in Internet stellen mussten, war eine Art Authentifizierung so gut wie unumgänglich. Damit wir keine Probleme mit Session-Affinity haben, entschieden wir uns für eine Token-Based Authentifizierung. Bei der genauen Implementation handelt es sich hier um das JSON Web Token, kurz JWT. 
			\begin{figure}[H]
				\centering
				\includegraphics[width=\linewidth]{img/backend/jwt.io.png}
				\caption{Aufbau eines JWT}
				\label{fig:aufbau-jwt}
			\end{figure}
		
			In Abbildung \ref{fig:aufbau-jwt} ist ein exemplarischer Aufbau eines JWT dargestellt. Das JWT besteht grundsätzlich aus drei Teilbereichen:
			\begin{enumerate}
				\item \textbf{Rot hinterlegt:} Bei diesem Teil handelt es sich um den Header, dieser beinhaltet den Typ des Tokens, als auch den Algorithmus, mit dem es verschlüsselt wurde.
				\item \textbf{Lila hinterlegt:} In diesem Teil werden die eigentlichen Nutzdaten des Tokens abgelegt, dort können z.B. Nutzernamen oder Nutzer-Id sowie eine Rolle hinterlegt werden.
				\item \textbf{Blau hinterlegt:} Der letzte Part ist dann noch die Signatur des Tokens.
			\end{enumerate}
			Jeder dieser Teile ist durch einen Punkt im Token abgetrennt. Es ist daher nicht verwunderlich, dass alle Token das selbe Präfix haben werden und nur der Mittelteil, sowie die Signatur sich ändern.
			
			Die Implementation in Spring Boot gelang in drei, vergleichsweise einfachen, Schritten. Zunächst mussten einige Konstanten definiert werden, zur einfacheren Handhabung wurde auch das Secret in den Code platziert. Dieses könnte aber sehr leicht über eine Umgebungsvariable überschrieben werden.
			
			\lstinputlisting[language=Java,caption=JWT Security Constants]{../backend/src/main/java/de/hft/geotime/security/SecurityConstants.java}
			
			Die Lebensdauer eines Tokens wurde mit 10 Tagen ebenfalls sehr hoch gewählt, um die Entwicklung zu vereinfachen. Auch muss dem Token zur erfolgreichen Nutzung in anderen Systemen das Prefix "Bearer " vorangestellt werden. 
			
			Um nun die Tokens in Java zu erzeugen und Abzugleichen, musste die Filterkette von Spring Boot, welche bei jedem Request durchlaufen wird, bearbeitet werden. Jeder Endpunkt außer "/login" und "/sign-up" benötigte ab diesem Zeitpunkt eine autorisierte Anfrage. 
			
			\lstinputlisting[language=Java,linerange={30-48},caption=JWT Authentication Filter,label=code:jwt-authentication-parse]{../backend/src/main/java/de/hft/geotime/security/JWTAuthenticationFilter.java}
			
			In Listing \ref{code:jwt-authentication-parse} ist der Schritt zu sehen, der die ankommende Anfrage versucht, in eine Loginanfrage zu parsen. Diese Anfrage wird dann in der Filterkette weitergereicht. Bis sie zum UserDetailsService kommt, welcher den User in der Datenbank abfragt und auch das Password abgleicht. Sollte die interne Autorisation erfolgreich sein, wird dieses Objekt mit den Nutzerdaten wieder an die Filterkette zurückgegeben und landet schließlich bei Listing \ref{code:jwt-authentication-create}.
			
			\lstinputlisting[language=Java,linerange={50-62},caption=JWT Authentication Filter,label=code:jwt-authentication-create]{../backend/src/main/java/de/hft/geotime/security/JWTAuthenticationFilter.java}
			
			Der letzte Schritt ist dann nur noch, das Token mit den erhaltenen Daten zu befüllen und dann den "Authorization" Header der Antwort auf das soeben erstelle Token zu setzen.
			
			Ab jetzt kann sich der Client, der das Token angefragt hat, für die nächsten 10 Tage damit authentifizieren. Dies läuft sehr ähnlich ab, deshalb hier nur sehr kurz dargestellt.
			
			\lstinputlisting[language=Java,linerange={40-55},caption=JWT Authorization Filter,label=code:jwt-authorization]{../backend/src/main/java/de/hft/geotime/security/JWTAuthorizationFilter.java}
			
			Der eingehende Request geht wieder durch die Filterkette und wenn er an dem Filter in Listing \ref{code:jwt-authorization} ankommt, wird der User extrahiert und später im Security Manager als Autorisation für diesen Request gesetzt. Wichtig ist hier, dass keine weitere Prüfung auf die Existenz des Users durchgeführt wird, auch das Password wird nicht nochmal abgefragt. Der Grund hierfür ist, wenn es den User nicht geben würde, wie käme er dann an das Token?
		\subsection{Repositories}
			Nachdem der Nutzer authentifiziert ist, bekommt er Zugriff auf alle REST-Repositories. Für jede Ressource, die oben im ER-Modell definiert ist gibt es ein entsprechendes Repository. Dieses wird größtenteils automatisch vom Classpath Scan von Spring automatisch implementiert. Die normalen CRUD Operationen werden für jedes angelegte Repository komplett ohne zutun implementiert. Ein solches Repository ist beispielsweise das der Location.
			
			\lstinputlisting[language=Java,firstline=8,caption=LocationRepository,label=code:location-repository]{../backend/src/main/java/de/hft/geotime/repositories/LocationRepository.java}
			
			Das einzige, was dort getan werden muss, ist die Angabe des Typs, der hier behandelt wird, hier Location, und der Datentyp des Primärschlüssels, hier ein Long. Die Bedeutung der Klasse "PagingAndSortingRepository" wird in einem späteren Kapitel genauer erläutert. Um den Link der Ressource anzupassen, werden die Parameter in der "RepositoryRestRessource" Annotation genutzt. Der Pfad geht immer vom Rootpfad der Applikation aus.
			
			Werden nun noch weitere Funktionalitäten in den Repositories benötigt, können diese entweder selbst implementiert werden, oder durch gut ausgewählte Funktionsdefinitionen im Interface der Ressource deklariert werden. Spring kann die Implementierung dann aus dem Namen und den Parametern der Funktion ableiten. Als unser Maximalbeispiel dient hier das RecordRepository.
			
			\lstinputlisting[language=Java,linerange={14-27,38-41,47-58},caption=RecordRepository,label=code:record-repository]{../backend/src/main/java/de/hft/geotime/repositories/RecordRepository.java}
			
			In diesem Repository befinden sich diverse verschiedene Methoden, wie Datenoperationen, die definiert werden können, ohne das sie aktiv implementiert werden müssen. Es beginnt mit der Funktion "findAllByStartdateBetween". Dieser Name kann direkt als Java Hibernate Statement interpretiert werden und nimmt als Parameter zwei Datumsangaben entgegen und eine Page. Die zwei Datumsangaben werden aus dem Schlüsselwort "Between" abgeleitet. Damit es sich aber um echt vergleichbare Daten handelt, müssen diese nach einem bestimmten Schema geparsed werden. Dieses Schema ist in der "DateTimeFormat" Annotation angegeben. Als Rückgabe liefert diese Funktion dann eine Menge aller Einträge zwischen diesen Daten.\\
			
			Die nächste Funktion funktioniert nun ähnlich, nur dass dort über Eigenschaften mehrerer verlinkter Objekte gegangen werden kann. "findAllBy" ist wieder das selbe wie oben und zeigt an, dass eine Liste von Ergebnissen zurückgeliefert wird, aber "Account\_User\_Username" bedeutet nun folgendes: "Gehe zum Account des Records, dann zum User dieses Accounts und von diesem User dann den Username". Der gefundene Username wird dann mit dem Parameter der Funktion verglichen und die Ergebnisse entsprechend gefiltert. Weiter zeigt das "And" eine Verkettung eines weiteren Ausdrucks an. So können auch relativ komplexe Abfragen automatisch implementiert werden.\\
			
			Reicht allerdings die obige Syntax nicht mehr aus, kann auch direkt eine Hibernate Abfrage über die "@Query" Annotation angegeben werden. Der Name der Funktion ist dann nicht mehr relevant für die Implementation, sondern nur noch für den Pfad, unter dem die Funktion später zu erreichen ist. Die Query an der "today" Funktion bietet nun die Möglichkeit, alle Einträge in der Records Tabelle für den aktuell anfragenden User zu bestimmen. Zusätzlich wird der Zeitraum noch auf den aktuellen Tag eingeschränkt, daher ergab sich auch der passende Name "today" für die Funktion. Der Nutzer wird automatisch über die "principal" Variable in der Abfrage eingefügt. Der Pricipal wird gesetzt, sobald der Authentication Filter den User erfolgreich eingeloggt hat. Weiter wird der aktuelle Tag über die Datenbankvariable "current\_date" abgefragt.\\
			
			Zuletzt kann auch nach Standard SQL Werten wie "null" oder "not null" gefragt werden. Zu sehen ist dies in der zuletzt dargestellten Funktion.\\
			
			Die Datei ist nicht vollständig abgedruckt, sondern nur ausschnittsweise, um die Grundkonzepte zu erläutern.
		\subsection{Projections}
			Projections bieten nun noch weitere Möglichkeiten, Daten vor der Rückgabe noch zu transformieren und gegebenenfalls mit Zusatzdaten anzureichern. Eine Projektion ist ebenfalls durch ein Interface definiert und bringt vor allem dann Vorteile, wenn mehrere Ressourcen gebündelt angefragt werden müssen, um beispielsweise eine Übersicht zu erstellen. 
			
			\lstinputlisting[language=Java,firstline=9,caption=RecordOverviewProjection,label=code:record-overview-projection]{../backend/src/main/java/de/hft/geotime/entities/projections/RecordOverviewProjection.java}
			
			Die "RecordOverviewProjection" (Listing \ref{code:record-overview-projection}) reichert eine normale "Record" Ressource noch zusätzlich mit dem Username und den Accountnamen an. Dadurch muss nicht für jeden Record erneut einzeln der Accountname nachgeschlagen werden. Zudem wird noch ein, bei jeder Anfrage neu berechnetes, zusätzliches Feld angefügt. Nämlich die Dauer des Records in Minuten.
			
			Eine Projektion kann am Beispiel des Records gut verdeutlicht werden.
			
			\begin{lstlisting}[language=json,caption=Einzelner Record ohne Projektion,label=code:records-without-proj]
{
	"startdate": "2020-05-30T18:00:00",
	"enddate": "2020-05-30T19:00:00",
	"type": "PAID",
	"duration": 60,
	"_links": {
		"self": {
			"href": "http://localhost:5000/records/27"
		},
		"records": {
			"href": "http://localhost:5000/records/27{?projection}",
			"templated": true
		},
		"account": {
			"href": "http://localhost:5000/records/27/account{?projection}",
			"templated": true
		}
	}
}
			\end{lstlisting}
			\begin{lstlisting}[language=json,caption=Einzelner Record mit Projektion "overview",label=code:records-with-proj]
{
	"duration": 60,
	"username": "scma",
	"account": "TestAccount",
	"startdate": "2020-05-30T18:00:00",
	"enddate": "2020-05-30T19:00:00",
	"type": "PAID",
	"_links": {
		"self": {
			"href": "http://localhost:5000/records/27"
		},
		"records": {
			"href": "http://localhost:5000/records/27{?projection}",
			"templated": true
		},
		"account": {
			"href": "http://localhost:5000/records/27/account{?projection}",
			"templated": true
		}
	}
}			
			\end{lstlisting}
			Es ist zu sehen, dass in Listing \ref{code:records-without-proj} die beiden Felder "account" und "username" fehlen, diese tauchen erst bei der Abfrage mit angewendeter, serverseitiger, Projektion auf (siehe Listing \ref{code:records-with-proj}). Die Anfrage für Listing \ref{code:records-without-proj} lautet \verb|http://localhost/records/27| und um nun die Projektion anzuhängen, wird die URL wie folgt erweitert: \verb|http://localhost/records/27?projection=overview|. Projektionen können hierbei nicht nur auf einzelne Objekte einer Ressource angewendet werden, sondern auch auf eine Menge dieser.
	\section{Endpoints}
		Die vier Hauptendpoints sind sicherlich die unserer Hautptressourcen: locations, accounts, records und users. Unten gibt es noch den nicht implementierten Endpoint für die Rollen ("roles"), dieser liefert aber kaum Informationen. Der "profile" Endpoint wird  erst im nächsten Kapitel erläutert. Um diesen Output zu bekommen, muss der Nutzer authentifiziert sein. Dies geschieht, wie oben schon erwähnt, über den "/login" Endpoint. Da dieser aber keine Ausgaben außer dem Header mit dem Token liefert, wird er hier nicht weiter erläutert. Selbiges gilt auch für den "/sign-up" Endpoint. Alle Anfragen, die ab jetzt ausgeführt werden, geschehen immer mit vorheriger Authentifizierung.
		\begin{lstlisting}[language=json,caption=Zugriff auf die Route "/" der API,label=code:main-route-api]
{
	"_links": {
		"locations": {
			"href": "http://localhost:5000/locations{?page,size,sort}",
			"templated": true
		},
		"accounts": {
			"href": "http://localhost:5000/accounts{?page,size,sort,projection}",
			"templated": true
		},
		"records": {
			"href": "http://localhost:5000/records{?page,size,sort,projection}",
			"templated": true
		},
		"users": {
			"href": "http://localhost:5000/users{?page,size,sort,projection}",
			"templated": true
		},
		"roles": {
			"href": "http://localhost:5000/roles"
		},
		"profile": {
			"href": "http://localhost:5000/profile"
		}
	}
}
		\end{lstlisting}
		
		\begin{lstlisting}[language=json,caption=Zugriff auf die Route "/locations" der API,label=code:locations-route-api]
{
	"_embedded": {
		"locations": [
			{
				"latitude": 1.0,
				"longitude": 2.0,
				"radius": 3,
				"_links": {
					"self": {
						"href": "http://plesk.icaotix.de:5000/locations/1"
					},
					"locations": {
						"href": "http://plesk.icaotix.de:5000/locations/1"
					}
				}
			}
		]
	},
	"_links": {
		"self": {
			"href": "http://plesk.icaotix.de:5000/locations{?page,size,sort}",
			"templated": true
		},
		"profile": {
			"href": "http://plesk.icaotix.de:5000/profile/locations"
		}
	},
	"page": {
		"size": 20,
		"totalElements": 6,
		"totalPages": 1,
		"number": 0
	}
}
		\end{lstlisting}
		
		Aufgrund der massiven Größe der Ausgaben der API werden die weiteren Endpoints nur noch mit ihrem Link angegeben. Alle Ressourcen unterstützen zudem die CRUD Operationen auf einzelnen Ressourcen, als auch auf der Hauptressource, deshalb werden sie aus Platzgründen ebenfalls übergangen.
		
		\subsection*{Endpoints für Ressourcen}
		\begin{itemize}
			\item \verb|/locations{?page,size,sort,projection}|
			\item \verb|/locations/<nr>{?projection}|
			\item \verb|/roles{?page,size,sort,projection}|
			\item \verb|/accounts{?page,size,sort,projection}|
			\item \verb|/accounts/<nr>{?projection}|
			\item \verb|/accounts/<nr>/user{?projection}|
			\item \verb|/accounts/search/findByUsernameAndName{?username,account,projection}|
			\item \verb|/accounts/search/findByUsername{?username,page,size,sort,projection}|
			\item \verb|/users{?page,size,sort,projection}|
			\item \verb|/users/<nr>{?projection}|
			\item \verb|/users/<nr>/location{?projection}|
			\item \verb|/users/<nr>/role{?projection}|
			\item \verb|/users/search/byUsername{?username,projection}|
			\item \verb|/records{?page,size,sort,projection}|
			\item \verb|/records/<nr>{?projection}|
			\item \verb|/records/<nr>/account{?projection}|
			\item \verb|/records/search/allBetweenAndUser{?start,end,username,page,size,sort,projection}|
			\item \verb|/records/search/openEntries{?page,size,sort,projection}|
			\item \verb|/records/search/today{?page,size,sort,projection}|
			\item \verb|/records/search/allForUser{?username,page,size,sort,projection}|
			\item \verb|/records/search/allBetween{?start,end,page,size,sort,projection}|
			\item \verb|/records/search/allFrom{?date,page,size,sort,projection}|
			\item \verb|/records/search/allForUserAndAccount{?username,account,page,size,sort,projection}|
		\end{itemize}
	
		Wenn Ressourcen aktualisiert werden müssen, müssen die Daten immer im JSON Format vorliegen. Die Felder des JSON Objekts müssen mit denen der Ressource übereinstimmen. Es müssen aber nicht alle Felder Werte beinhalten. Soll eine neue Ressource erstellt werden, werden die Daten als POST abgesendet, bei einer Aktualisierung als PATCH. Links zwischen Ressourcen können über den Self Link der Ressource hergestellt werden. Weiter gibt es noch zwei komplett selbst gebaute Endpoints.
		
		\subsection*{Der "/whoami" Endpoint}
			Dieser Endpoint dient dazu, um nach dem Login schnell die Startseite der App oder der Webseite mit den Nutzerdaten zu befüllen. Es sind Daten wie der Vor- und Nachname, sowie der Username enthalten. Zusätzlich wird noch die gesetzte Location des Nutzers mitgegeben.
			
		\subsection*{Der "/track" Endpoint}
			Beim "/track" Endpoint handelt es sich um einen der wichtigsten Endpoints. Er erlaubt es, ein Recording zu erstellen, ohne Angabe des Nutzers. Lediglich der Name des Timetrack Accounts, auf den gebucht werden soll, muss angegeben werden. Der Endpoint entscheidet auf Serverseite, von welchem Nutzer die Anfrage ankam. Dazu wird der Nutzer aus dem JWT extrahiert und abhängig davon im Account des Nutzers geschaut, ob schon ein Tracking läuft oder nicht. Sollte noch kein Tracking laufen, wird ein neuer Eintrag mit der aktuellen Zeit erstellt und zurückgeliefert. Das Enddatum ist zu diesem Zeitpunkt noch leer und auch die Duration zeigt "0" an. Sollte schon ein Tracking laufen, wird dieses mit der aktuellen Zeit als Endzeit beendet und ebenfalls zurückgeliefert. Sollte der Account nicht gefunden werden, oder ein anderer Fehler auftreten, wird ein entsprechender HTTP Statuscode zurückgeliefert.
			\begin{lstlisting}[language=json,caption=Aufruf von "/track" ohne laufendes Tracking,label=code:start-tracking-endpoint]
{
	"duration": 0,
	"username": "scma",
	"account": "Demo",
	"startdate": "2020-06-11T00:59:22",
	"enddate": null,
	"type": "PAID"
}
			\end{lstlisting}
			\begin{lstlisting}[language=json,caption=Aufruf von "/track" mit laufendem Tracking,label=code:stop-tracking-endpoint]
{
	"duration": 129,
	"username": "scma",
	"account": "Demo",
	"startdate": "2020-06-10T22:47:55",
	"enddate": "2020-06-11T00:57:41",
	"type": "PAID"
}
			\end{lstlisting}
		
		\subsection{HAL, Paging und Sorting}
			Die Hypertext Application Language, kurz HAL, ist eine Spezifikation, mit der APIs automatisch erkundbar gemacht werden können. Sie bietet META-Elemente an, einige davon werden auch bei uns benutzt.
			\begin{itemize}
				\item \textbf{"\_links"} zeigt weiterführende Links zu Ressourcen oder Informationen zum Paging an.
				\item \textbf{"\_embedded"} enthält die Nutzdaten zur entsprechenden Ressource, aber auch weitere Einbettungen zu Sub-Ressourcen.
			\end{itemize}
			Zusätzlich dazu nutzt Spring bei der Generierung der Repositories auch Teile der "Hypermedia as the Engine of Application State", kurz HATEOAS, Spezifikation. Das Listing \ref{code:main-route-api} zeigt hierfür den zusätzlichen Endpoint "profile". Unter diesem sind viele Spezifikationen zu finden, wie alle anderen Routen auf bestimmte Daten reagieren und auch antworten.
			
			Der "profile" Endpoint zeigt zusätzlich noch alle Projektionen an, die auf eine bestimmte Ressource angewendet werden können. Der Name der Projektion wird dann durch den URL-Parameter "projection=" angehängt.
			
			Zuletzt gilt es noch zu erwähnen, dass alle Ressourcen Paging und Sorting unterstützen. Paging ist besonders bei Web APIs wichtig, da die Geschwindigkeit sehr stark von der Menge der übertragenen Daten abhängt. Wenn eine Ressource immer alle Daten zurückliefern würde, würde dies bei mehreren hundert Einträgen sicher noch funktionieren. Aber sobald die Zahl der Einträge deutlich höher wird, muss abgeschnitten und aufgeteilt werden. Unsere Standard Seitengröße ist auf 20 Einträge gesetzt. Weiter enthält die Antwort des Servers durch die HAL Integration immer Links zur aktuellen, nächsten und vorherigen Seite als Link. Sorting wird ebenfalls unterstützt. Es kann nach jedem Feld im zurückgegebenen JSON sortiert werden, auch die Richtung ist spezifizierbar.
	\section{Probleme und Lösungen}
		\subsection{Einlesen in Spring}
			Spring ist ein sehr komplexes Framework, weshalb es manchmal wirklich sehr schwierig war, Fehler zu verstehen, und die Gründe dahinter zu verstehen. Solange man sich aber an viele der Best-Practices von Spring hält, ist es überhaupt nicht schwer, in relativ kurzer Zeit auch sehr komplexe APIs zu bauen. Durch die enorme Menge an Dokumentation und auch Hilfe aus der Community sowie Techtalks können viele Probleme leicht gelöst werden. 
		\subsection{Änderungen an den Endpoints}
			Es mussten anfangs viele Endpoints immer wieder umdefiniert werden, da sie nicht Best-Practices entsprochen haben oder nicht performant funktioniert haben. Dies wurde später aber immer einfacher, wenn man sich an die Denkweise einer REST-API gewöhnt hat. Auch zwei Wege Links zwischen Ressourcen waren bei uns nicht möglich, da sie zu Endlosrekursionen führten. Später wurde aber auch klar, das dies überhaupt nicht gewünscht ist.
		\subsection{Probleme mit MariaDB}
			Zu Beginn haben wir für das Docker-Image der Datenbank den "latest"-Tag benutzt. Dies war möglich, da wir keinerlei eigene Konfiguration der Datenbank und deren Tabellen vorgenommen haben. Allerdings wurde Mitte April die neue LTS-Version von Ubuntu veröffentlicht und damit auch das Basisimage von MariaDB angepasst. Durch Änderungen in Ubuntu 20.04 funktionierten nun gewisse Datenbankfunktionen nicht mehr ordnungsgemäß. Als Lösung kam dann nur ein Downgrade auf eine ältere Version in Frage. 
	\section{Deployment}
		Das Deployment des Backends findet immer gleichzeitig mit der Datenbank und dem Frontend statt. Die Daten bleiben dabei erhalten und werden, so fern nötig, von Spring automatisch migriert. Auch beim Hinzufügen oder Entfernen von Feldern aus Entitäten aktualisiert Spring die Datenbanktabellen entsprechend den neuen Feldern. Sollten Felder wegfallen, werden diese einfach gelöscht. Kommen neue hinzu, wird entweder der Defaultwert gesetzt oder, wenn erlaubt, "Null".