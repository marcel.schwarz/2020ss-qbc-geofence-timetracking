SET FOREIGN_KEY_CHECKS=0;

DELETE FROM role;
DELETE FROM location;
DELETE FROM timetrack_user;
DELETE FROM timetrack_account;

INSERT INTO role (id, `name`) VALUES
  (1, 'Admin');

INSERT INTO location (id, latitude, longitude, radius) VALUES
  (1, 48.804372, 9.521177, 50);

 /* password is the firstname in lowercase e.g. marcel or tobias
    https://bcrypt-generator.com/ with 10 rounds
 */
INSERT INTO timetrack_user (id, firstname, lastname, password, username, role_id, location_id) VALUES
  (1, 'Marcel', 'Schwarz' ,'$2y$10$pDBv7dEaAiNs5Kr1.8g4XuTFx48zGxJu77rei4TlO.sDOF2yHWxo.', 'scma', 1, 1),
  (2, 'Tobias', 'Wieck' ,'$2y$10$Fxj5cGrZblGKjIExvS/MquEE0lgyYo1ILxPgPR2vSiaaLKkqJ.C.u', 'wito', 1, 1),
  (3, 'Tim', 'Zieger' ,'$2y$10$pYGHZhoaelceImO7aIN4nOkWJBp.oqNGFYaRAonHkYF4u9ljqPelC', 'ziti', 1, 1),
  (4, 'Simon', 'Kellner' ,'$2y$10$Puzm/Nr/Dyq3nQxlkXGIfubS5JPtXJSOf2e6mrQ6HhVYQN9YiQQsC', 'kesi', 1, 1);

INSERT INTO timetrack_account (id, description, `name`, revenue, user_id) VALUES
  (1, 'Gleitzeit Marcel', 'Primary', 16.0, 1),
  (2, 'Festgeld Marcel', 'Secondary', 25.0, 1),
  (3, 'Festgeld Tim', 'Primary', 25.0, 3);

INSERT INTO time_record (id, enddate, startdate, `type`, account_id) VALUES
  (1, '2020-05-10 16:00:00', '2020-05-10 12:00:00', 0, 1),
  (2, '2020-05-09 16:00:00', '2020-05-09 12:00:00', 1, 1),
  (3, '2020-05-20 16:00:00', '2020-05-20 00:00:00', 1, 2),
  (4, '2020-05-11 16:00:00', '2020-05-11 12:00:00', 1, 1),
  (5, '2020-05-30 16:00:00', '2020-05-30 12:00:00', 1, 1),
  (6, '2020-05-30 23:00:00', '2020-05-30 22:00:00', 1, 1),
  (7, '2020-05-31 01:00:00', '2020-05-31 00:00:00', 1, 1),
  (8, '2020-05-31 04:00:00', '2020-05-31 02:00:00', 1, 1),
  (9, '2020-05-31 16:00:00', '2020-05-31 12:00:00', 1, 1),
  (10, '2020-06-14 16:00:00', '2020-06-14 12:00:00', 1, 1),
  (11, '2020-06-15 16:00:00', '2020-06-15 12:00:00', 1, 1);

SET FOREIGN_KEY_CHECKS=1;