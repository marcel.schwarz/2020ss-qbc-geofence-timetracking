package de.hft.geotime.repositories;

import de.hft.geotime.entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

}
