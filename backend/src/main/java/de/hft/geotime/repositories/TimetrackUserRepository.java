package de.hft.geotime.repositories;

import de.hft.geotime.entities.TimetrackUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.websocket.server.PathParam;

@RepositoryRestResource(
        path = "users",
        itemResourceRel = "users",
        collectionResourceRel = "users"
)
public interface TimetrackUserRepository extends PagingAndSortingRepository<TimetrackUser, Long> {

    @RestResource(path = "byUsername", rel = "byUsername")
    TimetrackUser findFirstByUsername(@PathParam("username") String username);

}
