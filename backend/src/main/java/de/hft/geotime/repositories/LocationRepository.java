package de.hft.geotime.repositories;

import de.hft.geotime.entities.Location;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(
        path = "locations",
        itemResourceRel = "locations",
        collectionResourceRel = "locations"
)
public interface LocationRepository extends PagingAndSortingRepository<Location, Long> {
}
