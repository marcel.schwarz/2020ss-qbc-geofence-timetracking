package de.hft.geotime.repositories;

import de.hft.geotime.entities.TimetrackAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(
        path = "accounts",
        itemResourceRel = "accounts",
        collectionResourceRel = "accounts"
)
public interface TimetrackAccountRepository extends PagingAndSortingRepository<TimetrackAccount, Long> {

    @RestResource(rel = "findByUsernameAndName", path = "findByUsernameAndName")
    TimetrackAccount findByUser_UsernameAndName(String username, String account);

    @RestResource(rel = "findByUsername", path = "findByUsername")
    Page<TimetrackAccount> findAllByUser_Username(String username, Pageable pageable);

}
