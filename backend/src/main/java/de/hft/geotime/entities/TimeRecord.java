package de.hft.geotime.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TimeRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TimetrackAccount account;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime startdate;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime enddate;

    private RecordType type;

    public long getDuration() {
        if (enddate == null) {
            return 0;
        } else {
            return startdate.until(enddate, ChronoUnit.MINUTES);
        }
    }

}
