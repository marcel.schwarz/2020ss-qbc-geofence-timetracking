package de.hft.geotime.entities.projections;

import de.hft.geotime.entities.TimetrackUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "withRole", types = TimetrackUser.class)
public interface UserWithRoleProjection {

    String getFirstname();

    String getLastname();

    String getUsername();

    @Value("#{target.role.name}")
    String getRole();

}
