package de.hft.geotime.entities.projections;

import de.hft.geotime.entities.TimetrackAccount;
import de.hft.geotime.entities.TimetrackUser;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "withUser", types = TimetrackAccount.class)
public interface AccountWithUserProjection {

    double getRevenue();

    String getName();

    String getDescription();

    TimetrackUser getUser();

}
