package de.hft.geotime.entities.projections;

import de.hft.geotime.entities.TimeRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "overview", types = TimeRecord.class)
public interface RecordOverviewProjection {

    LocalDateTime getStartdate();

    LocalDateTime getEnddate();

    long getDuration();

    @Value("#{target.type.name()}")
    String getType();

    @Value("#{target.account.name}")
    String getAccount();

    @Value("#{target.account.user.username}")
    String getUsername();

}
