package de.hft.geotime.entities.projections;

import de.hft.geotime.entities.TimetrackUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "onlyLocation", types = TimetrackUser.class)
public interface UserOnlyLocationProjection {

    @Value("#{target.location.longitude}")
    double getLongitude();

    @Value("#{target.location.latitude}")
    double getLatitude();

    @Value("#{target.location.radius}")
    int getRadius();

}
