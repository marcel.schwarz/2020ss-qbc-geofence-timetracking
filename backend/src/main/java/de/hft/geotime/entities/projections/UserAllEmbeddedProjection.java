package de.hft.geotime.entities.projections;

import de.hft.geotime.entities.Location;
import de.hft.geotime.entities.Role;
import de.hft.geotime.entities.TimetrackUser;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "allEmbedded", types = TimetrackUser.class)
public interface UserAllEmbeddedProjection {

    long getId();

    String getFirstname();

    String getLastname();

    String getUsername();

    Role getRole();

    Location getLocation();

}
