import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true
        },
        themes: {
            dark: {
                primary: '#0096ff',
                secondary: '#b0bec5',
                accent: '#8c9eff',
                error: '#b71c1c',
                
                
                main: '#272727',
                main_accent: '#202020',
                footer: '#404040',
                background: '#131313',
                background_soft: '#171717',
            },
        },
        dark:true
    },
});
