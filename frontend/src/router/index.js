import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import missing from "../views/missing.vue";
import TimeRecords from "../views/TimeRecords.vue";
import About from "../views/About.vue";
import StatisticOverview from "../views/StatisticOverview.vue";
import Users from "../views/Users.vue";
import EditUser from "../views/EditUser.vue";
import TimeTrackAccounts from "../views/TimeTrackAccounts.vue";
import EditTimeTrackAccount from "../views/EditTimeTrackAccount.vue"
import CreateTimeTrackAccount from "../views/CreateTimeTrackAccount.vue"
import EditTimerecord from "../views/EditTimerecord.vue"
import CreateTimerecord from "../views/CreateTimerecord.vue"
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "Geo Timetracking - Home",
    }
  },
  {
    path: "/timerecords",
    name: "TimeRecords",
    component: TimeRecords,
    meta: {
      title: 'Geo Timetracking - Time Records',
    }
  },

  {
    path: "/about",
    name: "About",
    component: About,
    meta: {
      title: 'Geo Timetracking - About',
    }
  },
  {
    path: "/statistics",
    name: "Statistics",
    component: StatisticOverview,
    meta: {
      title: 'Geo Timetracking - Statistics',
    }
  },
  {
    path: "/users",
    name: "Users",
    component: Users,
    meta: {
      title: 'Geo Timetracking - Users',
    }
  },
  {
    path: "/edituser",
    name: "EditUser",
    component: EditUser,
    meta: {
      title: 'Geo Timetracking - Edit User',
    }
  },
  {
    path: "/timetrackaccounts",
    name: "TimeTrack Accounts",
    component: TimeTrackAccounts,
    meta: {
      title: 'Geo Timetracking - TimeTrack Accounts',
    }
  },
  {
    path: "/edittimetrackaccount",
    name: "Edit TimeTrack Account",
    component: EditTimeTrackAccount,
    meta: {
      title: 'Geo Timetracking - Edit TimeTrack Account',
    }
  },
  {
    path: "/createtimetrackaccount",
    name: "Create TimeTrack Account",
    component: CreateTimeTrackAccount,
    meta: {
      title: 'Geo Timetracking - Create TimeTrack Accounts',
    }
  },
  {
    path: "/edittimerecord",
    name: "EditTimerecord",
    component: EditTimerecord,
    meta: {
      title: 'Geo Timetracking - Edit Time Record',
    }
  },
  {
    path: "/createtimerecord",
    name: "CreateTimerecord",
    component: CreateTimerecord,
    meta: {
      title: 'Geo Timetracking - Create Time Record',
    }
  },
  {
    path: '*',
    component: missing
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
