package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class EmbeddedRecords(records: ValuesRecordsArray) {
    @SerializedName("_embedded")
    var records = records
}