package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesUser(
    role: String,
    firstname: String,
    lastname: String,
    username: String,
    location: ValuesLocation,
    id: Int
) {

    @SerializedName("role")
    var role = role

    @SerializedName("firstname")
    var firstname = firstname

    @SerializedName("lastname")
    var lastname = lastname

    @SerializedName("username")
    var username = username

    @SerializedName("location")
    var location = location

    @SerializedName("id")
    var id = id

}