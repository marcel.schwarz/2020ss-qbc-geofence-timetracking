package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesTracking(
    duration: Int,
    start: String,
    end: String,
    account: String,
    user: String,
    type: String
) {
    @SerializedName("duration")
    var duration = duration

    @SerializedName("startdate")
    var startdate = start

    @SerializedName("enddate")
    var enddate = end

    @SerializedName("account")
    var account = account

    @SerializedName("username")
    var username = user

    @SerializedName("type")
    var type = type
}