package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesUserLogin(name: String, pswd: String) {

    @SerializedName("username")
    var username = name

    @SerializedName("password")
    var password = pswd

}