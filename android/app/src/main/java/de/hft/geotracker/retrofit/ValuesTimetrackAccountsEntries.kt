package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesTimetrackAccountsEntries(
    revenue: Double,
    name: String,
    description: String
) {

    @SerializedName("revenue")
    var revenue = revenue

    @SerializedName("name")
    var name = name

    @SerializedName("description")
    var description = description
}