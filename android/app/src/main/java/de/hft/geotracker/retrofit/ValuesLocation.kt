package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesLocation(
    latitude: Double,
    longitude: Double,
    radius: Int
) {

    @SerializedName("latitude")
    var latitude = latitude

    @SerializedName("longitude")
    var longitude = longitude

    @SerializedName("radius")
    var radius = radius.toFloat()
}