package de.hft.geotracker.activities

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.hft.geotracker.R
import kotlinx.android.synthetic.main.activity_home.*

class Settings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        my_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        findViewById<TextView>(R.id.button_submit).setOnClickListener {
        Toast.makeText(this@Settings, "Not yet implemented!", Toast.LENGTH_LONG)
            .show()
        }
    }

}