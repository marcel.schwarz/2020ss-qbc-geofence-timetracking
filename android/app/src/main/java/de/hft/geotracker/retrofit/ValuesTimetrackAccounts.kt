package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesTimetrackAccounts(entries: Array<ValuesTimetrackAccountsEntries>) {
    @SerializedName("accounts")
    var entries = entries
}