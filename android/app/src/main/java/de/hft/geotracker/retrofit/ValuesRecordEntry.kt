package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesRecordEntry(
    start: String,
    end: String,
    type: String,
    duration: Int
) {
    @SerializedName("startdate")
    var startdate = start

    @SerializedName("enddate")
    var enddate = end

    @SerializedName("type")
    var type = type

    @SerializedName("duration")
    var duration = duration
}