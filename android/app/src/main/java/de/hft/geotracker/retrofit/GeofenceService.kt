package de.hft.geotracker.retrofit

import retrofit2.Call
import retrofit2.http.*

interface GeofenceService {
    @POST("/login")
    fun login(@Body login_data: ValuesUserLogin): Call<Void>

    @GET("whoami")
    fun getUser(): Call<ValuesUser>

    @GET("accounts/search/findByUsername")
    fun getAccounts(@Query("username") username : String): Call<EmbeddedAccounts>

    @GET("track")
    fun triggerTracking(@Query("account") account: String): Call<ValuesTracking>

    @GET("records/search/today")
    fun getTodaysRecords(): Call<EmbeddedRecords>
}