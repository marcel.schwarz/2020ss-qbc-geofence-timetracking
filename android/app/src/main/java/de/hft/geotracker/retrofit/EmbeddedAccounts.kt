package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class EmbeddedAccounts(accounts: ValuesTimetrackAccounts) {
    @SerializedName("_embedded")
    var accounts = accounts
}