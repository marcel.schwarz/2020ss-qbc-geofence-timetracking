package de.hft.geotracker.retrofit

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(pToken: String) : Interceptor {
    private val token = pToken

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
            .header("Authorization", token)
        val request = builder.build()
        return chain.proceed(request)
    }

}