package de.hft.geotracker.retrofit

import com.google.gson.annotations.SerializedName

class ValuesRecordsArray(entries: Array<ValuesRecordEntry>) {
    @SerializedName("records")
    var entries = entries
}