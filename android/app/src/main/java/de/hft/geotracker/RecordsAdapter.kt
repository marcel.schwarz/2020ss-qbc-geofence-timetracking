package de.hft.geotracker

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import de.hft.geotracker.activities.RecordEntry

class RecordsAdapter : RecyclerView.Adapter<TextItemViewHolder>() {
    var data = listOf<RecordEntry>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater
            .inflate(R.layout.text_item_view, parent, false) as CardView
        return TextItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: TextItemViewHolder, position: Int) {
        val item = data[position]
        holder.textFrom.setText("Start: " + item.from)
        holder.textTo.setText("End: " + item.to)
        if (item.duration != -1) {
            holder.textTotal.setText("Duration: " + item.duration)
        }

    }

}

class TextItemViewHolder(textView: CardView): RecyclerView.ViewHolder(textView) {
    val textFrom = itemView.findViewById<TextView>(R.id.recyclerText_from)
    val textTo = itemView.findViewById<TextView>(R.id.recyclerText_to)
    val textTotal = itemView.findViewById<TextView>(R.id.recyclerText_total)
}